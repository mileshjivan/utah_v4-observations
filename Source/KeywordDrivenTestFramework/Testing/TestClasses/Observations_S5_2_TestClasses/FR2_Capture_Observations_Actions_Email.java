/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses.Observations_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Observations_V5_2_PageObjects.Observations_V5_2_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Delwin.Horsthemke
 */
@KeywordAnnotation(
        Keyword = "FR2_Capture_Observations_Actions_Email",
        createNewBrowserInstance = true
)

public class FR2_Capture_Observations_Actions_Email extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR2_Capture_Observations_Actions_Email()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!ViewandOpenEngagementPlanActionsEmail())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Completed Capture Engagement_Plan");
    }

    public boolean ViewandOpenEngagementPlanActionsEmail(){
        //Launch Office.com
        if (!SeleniumDriverInstance.navigateTo(Observations_V5_2_PageObjects.office_URL())) {
        error = "Failed to navigate to office 365 home Page.";
        return false;
        }

        String parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();

        pause(5000);
        narrator.stepPassedWithScreenShot("Successfully opened Office website");
        
        //Signin to office account
        if(!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.office_signin())){
        error = "Failed to wait for office signin ";
        return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.office_signin())){
        error = "Failed to click offici signin button";
        return false;
        }
        
        //Email address
        pause(3000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.office_email_id())){
        error = "Failed to wait for Email address field";
        return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Observations_V5_2_PageObjects.office_email_id(),testData.getData("Username"))){
        error = "Failed to enter text in Email address field";
        return false;
        }
        
        //Next button
        if(!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.email_next_btn())){
        error = "Failed to wait for Next button";
        return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.email_next_btn())){
        error = "Failed to click Next button";
        return false;
        }
        
        //Password field
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.office_password())){
        error = "Failed to wait for Password field";
        return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Observations_V5_2_PageObjects.office_password(),testData.getData("Password"))){
        error = "Failed to enter text in Password field";
        return false;
        }
        
        //Signin button
        if(!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.office_signin_btn())){
        error = "Failed to wait for Signin button";
        return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.office_signin_btn())){
        error = "Failed to click Signin button";
        return false;
        }
        
//        //No button
//        if(!SeleniumDriverInstance.waitForElementByXpath(Medical_Surveillance_PageObjects.office_No_btn())){
//        error = "Failed to wait for No button";
//        return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Medical_Surveillance_PageObjects.office_No_btn())){
//        error = "Failed to click No button";
//        return false;
//        }
        
        //outlook icon
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.outlook_icon())){
        error = "Failed to wait for outlook icon";
        return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.outlook_icon())){
        error = "Failed to click outlook icon";
        return false;
        }
        
        //switch to new window
        if(!SeleniumDriverInstance.switchToWindow()){
        error = "Failed to switch to new window or tab.";
        return false;
        }
        
//        //inbox chevron expand
//        if(!SeleniumDriverInstance.waitForElementByXpath(FR1_Capture_Actions_MainScenario_Email_Action_Logged_PageObjects.inbox_chevron_expand())){
//        error = "Failed to wait for inbox chevron expand";
//        return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(FR1_Capture_Actions_MainScenario_Email_Action_Logged_PageObjects.inbox_chevron_expand())){
//        error = "Failed to click inbox chevron expand";
//        return false;
//        }
        
        //system mail folder
        if(!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.system_mail_folder())){
        error = "Failed to wait for inbox chevron expand";
        return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.system_mail_folder())){
        error = "Failed to click inbox chevron expand";
        return false;
        }
        
        pause(10000);
        
        //Email notification
        if(!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.email_notification_Actions())){
        error = "Failed to wait for Email notification";
        return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.email_notification_Actions())){
        error = "Failed to click on Email notification";
        return false;
        }
        
        //Link back to record
        if(!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.linkBackToRecord_Link())){
        error = "Failed to wait for Link back to record";
        return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.linkBackToRecord_Link())){
        error = "Failed to click on Link back to record";
        return false;
        }
        
        pause(15000);
        //switch to new window
        if(!SeleniumDriverInstance.switchToWindow(SeleniumDriverInstance.Driver,"IsoMetrix")){
        error = "Failed to switch to new window or tab.";
        return false;
        }
        
        pause(5000);

        //Isometrix Username
        if(!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Username())){
        error = "Failed to wait for username";
        return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Observations_V5_2_PageObjects.Username(),testData.getData("IsoMetrixUsername"))){
        error = "Failed to enter username";
        return false;
        }
        
        //Isometrix Password
        if(!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Password())){
        error = "Failed to wait for Password";
        return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Observations_V5_2_PageObjects.Password(),testData.getData("IsoMetrixPassword"))){
        error = "Failed to enter Password";
        return false;
        }
        
        //Isometrix SignIn button
        if(!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.LoginBtn())){
        error = "Failed to wait for Signin";
        return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.LoginBtn())){
        error = "Failed to click Signin";
        return false;
        }

        pause(15000);

        narrator.stepPassedWithScreenShot("Successfully opened the record");
        
        return true;
        
    } 
}
