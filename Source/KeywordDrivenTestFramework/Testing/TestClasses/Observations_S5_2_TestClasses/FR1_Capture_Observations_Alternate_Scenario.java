/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Observations_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import KeywordDrivenTestFramework.Testing.PageObjects.Observations_V5_2_PageObjects.Observations_V5_2_PageObjects;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Observations v5.2 - Alternate Scenario",
        createNewBrowserInstance = false
)
public class FR1_Capture_Observations_Alternate_Scenario extends BaseClass {
    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Observations_Alternate_Scenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Closed_Observations()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully");
    }

    public boolean Closed_Observations() {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Observations_ProcessFlow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }

        //status
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Status())) {
            error = "Failed to wait for status";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Status())) {
            error = "Failed to click on status";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Status_Option(getData("Status")))) {
            error = "Failed to wait for status option";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Status_Option(getData("Status")))) {
            error = "Failed to click on status option";
            return false;
        }
        narrator.stepPassedWithScreenShot("Status: " + getData("Status"));

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.saveBtn())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.saveBtn())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }

        //Saving mask
        if (SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Observations_V5_2_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        //Validation
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.inspection_Record_Saved_popup())) {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Observations_V5_2_PageObjects.inspection_Record_Saved_popup());
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.inspection_Record_Saved_popup())) {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Successfully clicked save");
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.failed())) {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Observations_V5_2_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved")) {
                error = "Failed to save record.";
                return false;
            }
        }
        return true;
    }
}
